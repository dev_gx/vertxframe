package com.guo.mixframe.messages

import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec

/**
 * @author  gx
 * @description
 */
class ByteArrayCodec : MessageCodec<ByteArray,ByteArray> {
  override fun encodeToWire(buffer: Buffer, orderMessage: ByteArray?) {
    buffer.appendBytes(orderMessage)
  }

  override fun decodeFromWire(pos: Int, buffer: Buffer): ByteArray? {
    return buffer.bytes
  }

  override fun transform(orderMessage: ByteArray?): ByteArray? {
    return orderMessage
  }

  override fun name(): String {
    return this::class.toString()
  }

  override fun systemCodecID(): Byte {
    return -1
  }

  companion object {
    fun create(): MessageCodec<ByteArray, *> {
      return ByteArrayCodec()
    }
  }
}
