package com.guo.mixframe.messages

import com.guo.mixframe.framework.exceptions.CustomException
import io.vertx.core.MultiMap
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message

/**
 * @author  gx
 * @description 请求处理基类
 */
open class BaseRequestWrapper(
  val url: String, val headers: MultiMap, val message: Message<ByteArray>
) {
  var vertx:Vertx? = null


  open suspend fun doInit(){

  }

  open fun checkZone():String?{
    return null
  }

  fun ip():String{
    return headers["ip"]
  }


  fun sendError(exception: CustomException){
    message.fail(exception.code, exception.message)
  }



}
