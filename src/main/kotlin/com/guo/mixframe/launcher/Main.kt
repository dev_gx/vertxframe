package com.guo.mixframe.launcher

import com.guo.mixframe.framework.annotations.LoadStatic
import com.guo.mixframe.framework.annotations.Receiver
import com.guo.mixframe.framework.configs.CoreConfig
import com.guo.mixframe.framework.log.LogPrint
import com.guo.mixframe.framework.receivers.GameMessageHelper
import com.guo.mixframe.framework.receivers.rpc.RpcClient
import com.guo.mixframe.framework.receivers.rpc.RpcReceiver
import com.guo.mixframe.framework.receivers.rpc.RpcServer
import com.guo.mixframe.framework.receivers.rpc.RpcService
import com.guo.mixframe.framework.receivers.websocket.WebSocketServer
import com.guo.mixframe.framework.redis.RedisAdapter
import com.guo.mixframe.framework.urls.URLMapping
import com.guo.mixframe.framework.utils.PackageUtils
import com.guo.mixframe.messages.ByteArrayCodec
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions

/**
 * @author  gx
 * @description 主入口
 */
class Main {


  companion object {

    private lateinit var vertx: Vertx

    @JvmStatic
    fun main(args: Array<String>) {
      LogPrint.logger.debug("start")
      val op = VertxOptions()
      if (CoreConfig.getInstance().serverOpen()) {
        op.blockedThreadCheckInterval = 999999999999
      }
      vertx = Vertx.vertx(op)
      vertx.eventBus().registerDefaultCodec(ByteArray::class.java, ByteArrayCodec.create())
      loadStatic()
      URLMapping.initMethodUrls()
      RedisAdapter.instance.init(vertx)


      deployReceivers()

      deployRpc(vertx)
    }

    private fun deployRpc(vertx: Vertx) {
      RpcService.deployRpcReceiver(vertx)
      RpcService.deployRpcDispatcher(vertx)
      RpcClient.instance.init(vertx)
    }



    /**
     * 网络接收器
     */
    private fun deployReceivers() {
      val classes = PackageUtils.getClassesByAnnotation("com.guo.mixframe", Receiver::class)
      val dop = DeploymentOptions()
      dop.instances = vertx.nettyEventLoopGroup().count() / 2 //每个cpu创建一个receiver实例
      for (klass in classes) {
        LogPrint.logger.debug("Deploy receiver: ${klass.qualifiedName}")//打印全包名路径
      }

      val netMessageHelper = GameMessageHelper(vertx.eventBus())
      val coreConfig = CoreConfig.getInstance()
      if (CoreConfig.getInstance().checkUseWebSocket()) {
        vertx.deployVerticle( WebSocketServer(coreConfig.checkKcpPort(),coreConfig.checkTimeOut(),netMessageHelper) , dop)
      }else if(CoreConfig.getInstance().checkHttp()){

      }else{
        /*启动tcp和kcp服务*/
      }

    }

    /**
     * 项目启动初始化静态资源
     */
    private fun loadStatic() {
      val classes = PackageUtils.getClassesByAnnotation("com.guo.mixframe", LoadStatic::class)
      for (cls in classes) {
        LogPrint.logger.debug("Loading class ${cls.qualifiedName}")
        try {
          Class.forName(cls.qualifiedName)
        } catch (e: Exception) {
          LogPrint.logger.error("Load class ${cls.qualifiedName} failed.", e)
        } catch (e: ExceptionInInitializerError) {
          LogPrint.logger.error("Load class ${cls.qualifiedName} failed.", e)
        }
      }
    }


  }


}
