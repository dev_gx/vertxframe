package com.guo.mixframe.framework.session

import com.guo.mixframe.framework.receivers.GameClient

/**
 * @author  gx
 * @description session对象
 */
class SessionObject {

  var session: String? = null //session标识

  var userName: String? = null //玩家注册名或者sdk返回的登录名

  var playerId: String? = null //玩家在游戏中的唯一标识

  var zone: String? = null //玩家所在分区

  var nickName: String? = null //角色昵称

  var symKey64: String? = null //消息加密key

  var symIV64: String? = null //消息加密IV

  var channel: String? = null //玩家所在渠道

  var zoneTag: String? = null //玩家所在的zoneTag标识

  var platform: String? = null //设备类型：Android, Ios, Windows

  var installId: String? = null //设备唯一广告id

  var os: String? = null //操作系统

  var device: String? = null //设备详情

  var createTime: Long = 0 //创角时间

  var RegTime: Long = 0 //游戏内注册时间（第一次登陆时间）

  var loginTime: Long = 0 //登录时间

  var udid: String? = null //设备唯一标识

  var version: String? = null //游戏版本，验证用

  var opTime: Long = 0 //操作时间


  @Transient
  lateinit var key //加密key, 缓存下来给广播用, 同时也避免了多次反序列化
    : ByteArray

  @Transient
  var client: GameClient? = null //跟玩家的连接


  fun getUniqueId(): String? { //KcpReceiver中排队使用
    return String.format("%s_%s", zone, playerId)
  }


}
