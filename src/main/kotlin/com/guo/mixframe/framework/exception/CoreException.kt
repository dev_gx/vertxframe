package com.guo.mixframe.framework.exception

import com.guo.mixframe.framework.log.LogPrint

/**
 * @author  gx
 * @description
 */
class CoreException : RuntimeException() {

  private val serialVersionUID = 2332608236621015980L


  fun throwException(code: Int) : CoreException{
    LogPrint.exceptionLogger.debug("Exception code is $code")
    return this
  }

  companion object{
    val invalidUrl: Int = 1


    val NoSessionError = 802 //玩家未登陆或者在其他服务器登录

    val UnknownError = 803 //未知错误

    val SessionExpired = 804 //玩家未登陆或者在其他服务器登录

    val TimeOut = 805 //超时

    val RedisError = 806 //redis连接异常

    val PermissionDenied = 807 //无权限，用于rpc内部判断和web后台请求

    val RpcFailed = 808 //rpc失败

    val ZoneNotExist = 809 //分区不存在

    val ZoneHasNoLocalIP = 810 //localIp未定义

    val ReadDataError = 811 //数据读取错误

    val IpAreaErr = 811 //该区域被禁止登录

    val InvalidVersion = 812 //版本错误

    val InvalidPayItem = 813 //支付金额和支付项不匹配

    val InvalidParam = 814 //参数错误

    val PayProductNotExist = 815 //不存在的支付项

    val ConfigError = 816 //配置错误

    val ServerMaintainance_Alpha = 817 //alpha分区维护中

    val ServerMaintainance_Beta = 818 //beta分区维护中

    val ServerMaintainance_ReleaseCandidate = 819 //预发布分区维护中

    val ServerMaintainance_Release = 820 //发布分区维护中

    val ServerMaintainance = 821 //系统维护中

    val UnSupportFunc = 822 //该方法未实现

    val GameServerBusy = 823 //服务器繁忙

    val SystemUnlocked = 824 //系统未解锁

    val NotSameZoneType = 825 //分区类型不一致

    val NotAllowCreateAdvancedRole = 826 //不允许创建先遣分区角色
  }



}
