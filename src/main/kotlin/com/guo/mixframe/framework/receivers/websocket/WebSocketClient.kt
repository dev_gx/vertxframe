package com.guo.mixframe.framework.receivers.websocket

import com.guo.mixframe.framework.receivers.GameClient
import io.netty.buffer.ByteBuf
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.ServerWebSocket

/**
 * @author  gx
 * @description
 */
class WebSocketClient(private val ws:ServerWebSocket) : GameClient{

  /*客户端ip*/
  override val remoteAddress:String get() = ws.remoteAddress().host()

  /*头4字节是否为全消息长度*/
  override val isDataWithLength: Boolean
    get() = false

  override var sessionId: String? = null

  /*
   * 是否支持消息推送
   */
  override val canBroadcast: Boolean
    get() = true


  override fun sendData(buffer: ByteBuf){
      ws.write(Buffer.buffer(buffer)){
        buffer.release()
      }
  }


}
