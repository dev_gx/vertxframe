package com.guo.mixframe.framework.receivers

import io.vertx.core.eventbus.DeliveryOptions

/**
 * 玩家请求队列数据
 */
data class GameMessage(
        val requestId: Long,
        val url: String?,
        val language: String?,
        val options: DeliveryOptions?,
        val body: ByteArray,
        val client: GameClient,
        val uniqueId: String?
)
