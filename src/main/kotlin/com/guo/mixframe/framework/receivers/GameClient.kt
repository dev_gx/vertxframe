package com.guo.mixframe.framework.receivers

import io.netty.buffer.ByteBuf

/**
 * @author  gx
 * @description
 */
interface GameClient {

  val remoteAddress: String //ip

  val isDataWithLength: Boolean //头部是否包含消息总长度字节数

  var sessionId: String? //sessionId

  fun sendData(buffer: ByteBuf)  //发数据到客户端


  val canBroadcast: Boolean //是否支持广播，http不支持, tcp和Kcp支持



}
