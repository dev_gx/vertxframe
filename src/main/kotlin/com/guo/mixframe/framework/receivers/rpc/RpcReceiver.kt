package com.guo.mixframe.framework.receivers.rpc

import com.guo.mixframe.framework.configs.CoreConfig
import io.vertx.core.AbstractVerticle
import io.vertx.core.json.JsonObject

/**
 * @author  gx
 * @description
 */
class RpcReceiver : AbstractVerticle(){

  override fun start() {
    val coreConfig = CoreConfig.getInstance()
    val rpcServer = RpcServer(coreConfig.checkRpcPort(), vertx)
    rpcServer.idleTimeout = coreConfig.checkTimeOut()
    rpcServer.sendTimeout = coreConfig.checkTimeOut().toLong()
    rpcServer.start()
  }
}
