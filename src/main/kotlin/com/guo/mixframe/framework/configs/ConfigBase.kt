package com.guo.mixframe.framework.configs

import com.guo.mixframe.framework.io.Loader
import com.guo.mixframe.framework.utils.FileHelper
import io.vertx.core.cli.annotations.DefaultValue
import io.vertx.core.json.JsonObject


/**
 * @author  gx
 * @description .json配置基类
 */
abstract class ConfigBase : Loader() {

  private lateinit var configs: Map<String,Any>

  abstract fun  checkConfigFilePath():String

  override fun reload() {
    useWriteLock {
      val json = FileHelper.readStringFromClassesPath(checkConfigFilePath())
      configs = JsonObject(json).map
    }
  }

  @Suppress("UNCHECKED_CAST")
  fun <T> loadValue(key:String,defaultValue: T) : T{
    return useReadLock { configs.getOrDefault(key,defaultValue) as T }
  }

}
