package com.guo.mixframe.framework.configs

import com.google.common.base.MoreObjects

/**
 * 逻辑服务器记录，以nameId为唯一标识符
 */
open class LogicServer {
    var nameId = ""          //分区唯一标识
    var tag = ""             //分区tag标识，表示哪些tag的包可以进入对应分区
    var chName = ""          //显示名称
    var isPublic = false     //该分区是否已对外开发
    var url = ""             //玩家访问分区的url
    var localIP = ""         //局域网ip
    var loadStatus = 0       //分区状态，0：推荐，1：爆满
    var redisIp = ""         //redisIp

    override fun toString(): String {
        val th = MoreObjects.toStringHelper(this)
                .add("nameId", nameId)
                .add("tag", tag)
                .add("chName", chName)
                .add("isPublic", isPublic)
                .add("url", url)
                .add("localIP", localIP)
                .add("loadStatus", loadStatus)
                .add("redisIp", redisIp)
        return th.toString()
    }

    fun copyFrom(server: LogicServer) {
        nameId = server.nameId
        tag = server.tag
        chName = server.chName
        isPublic = server.isPublic
        redisIp = server.redisIp
        loadStatus = server.loadStatus
        url = server.url
        localIP = server.localIP
    }

    companion object {
        const val STATUS_RECOMMEND = 0
        const val STATUS_NORMAL = 1
        const val STATUS_HIGHLOAD = 2
        const val STATUS_FULL = 3
    }
}
