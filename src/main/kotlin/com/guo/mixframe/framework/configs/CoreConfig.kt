package com.guo.mixframe.framework.configs

import com.guo.mixframe.framework.io.Loader
import com.guo.mixframe.framework.urls.URLType
import java.lang.Appendable

/**
 * @author  gx
 * @description 核心配置
 */
class CoreConfig : ConfigBase() {

  companion object {

    private val instance: CoreConfig

    @JvmStatic
    fun getInstance(): CoreConfig {
      return getInstance(instance, CoreConfig::class.java)
    }

    init {
      instance = getInstance()
    }
  }

  override fun checkConfigFilePath(): String {
    return "src/main/resources/config/coreConfig.json"
  }

  fun serverOpen(): Boolean {
    return loadValue("Enabled", false)
  }

  fun checkUseWebSocket(): Boolean {
    return loadValue("userWebSocket", false)
  }

  fun checkKcpPort(): Int {
    return loadValue("kcpPort", 8889)
  }

  fun checkTimeOut(): Int {
    return loadValue("timeOu", 5000)
  }

  fun checkHttp(): Boolean {
    return loadValue("userHttp", false)
  }

  fun isUrlTypeValid(urlType: Int): Boolean {
    when (urlType) {
      URLType.Login -> return isLogin
      URLType.Game -> return isGame
      URLType.Cross -> return isCross
      URLType.Web -> return isWeb
      URLType.SDK -> return isSDK
      URLType.Resource -> return isResource
    }
    return false
  }

  /**
   * 本机是否为游戏服
   */
  val isGame: Boolean
    get() = loadValue("server.game", false)

  /**
   * 本机是否为登陆服
   */
  val isLogin: Boolean
    get() = loadValue("server.login", false)

  /**
   * 本机是否为web后台服
   */
  val isWeb: Boolean
    get() = loadValue("server.web", false)

  /**
   * 本机是否为sdk服
   */
  val isSDK: Boolean
    get() = loadValue("server.sdk", false)

  /**
   * 本机是否为资源服
   */
  val isResource: Boolean
    get() = loadValue("server.resource", false)

  /**
   * 本机是否为中心服(跨服)
   */
  val isCross: Boolean
    get() = loadValue("server.cross", false)

  fun checkRpcPort(): Int {
      return loadValue("rpcPort",8888)
  }

  fun checkLoginIp(): String {
    return loadValue("loginIp","")
  }

  fun checkCrossIp(): String {
    return loadValue("crossIp","")
  }

  fun checkResourceIP(): String {
    return loadValue("resourceIP","")
  }


}
