package com.guo.mixframe.framework.urls

import kotlin.reflect.KCallable
import kotlin.reflect.KClass

/**
 * @author  gx
 * @description
 */
data class URLRecord(val klass: KClass<*>, val method: KCallable<*>, val requestType: KClass<*>, val urlType: Int)
