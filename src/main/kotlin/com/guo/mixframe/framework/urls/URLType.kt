package com.guo.mixframe.framework.urls

/**
 * url类型常量定义
 */
object URLType {
    const val Login: Int = 1      //登录
    const val Game: Int = 2       //游戏
    const val Resource: Int = 3   //资源
    const val Web: Int = 4        //web后台
    const val SDK: Int = 5        //SDK
    const val Cross: Int = 6      //中心服（跨服）
}
