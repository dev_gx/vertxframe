package com.guo.mixframe.framework.log

import org.slf4j.LoggerFactory


/**
 * @author  gx
 * @description 日志工具
 */
class LogPrint {

  companion object{
    val logger = LoggerFactory.getLogger("common")
    val exceptionLogger = LoggerFactory.getLogger("unexpected_throw")
    val logicFlowExceptionLogger = LoggerFactory.getLogger("logic_throw")
  }
}
