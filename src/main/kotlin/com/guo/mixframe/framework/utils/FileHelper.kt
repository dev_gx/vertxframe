package com.guo.mixframe.framework.utils

import com.guo.mixframe.framework.log.LogPrint
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStreamReader

/**
 * @author  gx
 * @description
 */
class FileHelper {


    companion object{
      /**
       * 读文件
       */
      fun readStringFromClassesPath(filename: String): String {
        val inputStream = FileInputStream(filename)
        val inputStreamReader = InputStreamReader(inputStream)
        try {
          val br = BufferedReader(inputStreamReader)
          val sb = StringBuilder()
          var s: String?
          while (br.readLine().also { s = it } != null) {
            sb.append(s)
            sb.append("\n")
          }
          br.close()
          return sb.toString()
        } catch (e: Exception) {
          LogPrint.exceptionLogger.error("", e)
          return ""
        }
      }

      fun getFullClassPath(filename:String):String{
        val path = FileHelper::class.java.getResource("/").path
        return path + filename
      }


    }


}
