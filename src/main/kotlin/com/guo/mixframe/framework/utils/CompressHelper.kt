package com.guo.mixframe.framework.utils

import org.apache.poi.util.IOUtils
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

/**
 * 压缩帮助类
 */
object CompressHelper {
    /**
     * 压缩
     */
    @JvmStatic
    fun compress(content: ByteArray): ByteArray {
        try {
            ByteArrayOutputStream().use { outputStream ->
                GZIPOutputStream(outputStream).use { gzip -> gzip.write(content) }
                return outputStream.toByteArray()
            }
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    /**
     * 解压
     */
    @JvmStatic
    fun decompress(contentBytes: ByteArray): ByteArray {
        try {
            ByteArrayOutputStream().use { outputStream ->
                ByteArrayInputStream(contentBytes).use { inputStream ->
                    GZIPInputStream(inputStream).use { gzip ->
                        IOUtils.copy(gzip, outputStream)
                        return outputStream.toByteArray()
                    }
                }
            }
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }
}
