package com.guo.mixframe.framework.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.guo.mixframe.framework.log.LogPrint
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.Map

/**
 * Json工具类
 */
object JsonHelper {
    /**
     * Json序列化
     */
    @JvmStatic
    fun toJson(obj: Any): String {
        return Gson().toJson(obj)
    }

    /**
     * Json反序列化
     */
    fun <T> fromJson(text: String, clazz: Class<T>): T {
        var tmpT = text
        //在lua操作时，会把空数组对象序列化为{}
        if (tmpT.contains("{}")){
            tmpT = tmpT.replace("{}", "[]")
        }
        return Gson().fromJson(tmpT, clazz)
    }

    fun fromJson(text: String): Map<*, *>? {
        var tmpT = text
        //在lua操作时，会把空数组对象序列化为{}
        if (tmpT.contains("{}")){
            tmpT = tmpT.replace("{}", "[]")
        }
        return Gson().fromJson(tmpT, java.util.Map::class.java)
    }
    /**
     * Json反序列化
     */
    fun <T> fromJson(bytes: ByteArray, clazz: Class<T>): T {
        return Gson().fromJson(String(bytes, charset("UTF-8")), clazz)
    }

    /**
     * Json序列化
     */
    fun toPrettyJson(obj: Any): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(obj)
    }

    /**
     * Json序列化为字节数组
     */
    fun toJsonBytes(obj: Any): ByteArray {
        return toJson(obj).toByteArray(StandardCharsets.UTF_8)
    }

    /**
     * Json序列化为base64
     */
    fun toBase64JsonBytes(obj: Any): ByteArray {
        try {
            val jsonStr = toJson(obj)
            return Base64.getEncoder().encode(jsonStr.toByteArray(StandardCharsets.UTF_8))
        } catch (e: Exception) {
            LogPrint.exceptionLogger.error("", e)
            throw e
        }
    }

    /**
     * Json序列化为压缩格式的字节数组
     */
    fun toCompressedJsonBytes(obj: Any): ByteArray {
        return CompressHelper.compress(toJsonBytes(obj))
    }

    /**
     * Json序列化为压缩格式的字符串
     */
    fun toCompressedJson(obj: Any): String {
        return try {
            String(toCompressedJsonBytes(obj), StandardCharsets.ISO_8859_1)
        } catch (e: Exception) {
            LogPrint.exceptionLogger.error("", e)
            ""
        }
    }

    /**
     * 从压缩Json字符串反序列化
     */
    fun <T> fromCompressedJson(compressedStr: String, clazz: Class<T>): T {
        return try {
            fromCompressedJson(compressedStr.toByteArray(StandardCharsets.ISO_8859_1), clazz)
        } catch (e: Exception) {
            LogPrint.exceptionLogger.error("", e)
            throw e
        }
    }

    /**
     * 从压缩Json字节数组反序列化
     */
    fun <T> fromCompressedJson(compressedBytes: ByteArray, clazz: Class<T>): T {
        return fromJson(CompressHelper.decompress(compressedBytes), clazz)
    }

    /**
     * 从压缩Base64字节数组反序列化
     */
    fun <T> fromCompressedBase64Json(compressedBytes: ByteArray, clazz: Class<T>): T {
        val base64Bytes = CompressHelper.decompress(compressedBytes)
        val bytes = Base64.getDecoder().decode(base64Bytes)
        val decodeStr = String(bytes, StandardCharsets.UTF_8)
        return fromJson(decodeStr, clazz)
    }
}
