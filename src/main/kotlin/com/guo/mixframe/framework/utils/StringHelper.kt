package com.guo.mixframe.framework.utils

import com.guo.mixframe.framework.log.LogPrint
import java.lang.Exception
import java.nio.charset.StandardCharsets

/**
 * @author  gx
 * @description
 */
object StringHelper {

  /**
   * 判断字符串是否为null和空
   */
  @JvmStatic
  fun isNullOrEmpty(str: String?): Boolean {
    return str == null || str.isEmpty()
  }

  /**
   * 将字节数组转换为字符串
   */
  @JvmStatic
  fun bytesToString(bytes: ByteArray): String {
    return String(bytes, StandardCharsets.ISO_8859_1)
  }

  /**
   * 将字符串转换为字节数组
   */
  @JvmStatic
  fun stringToBytes(str: String): ByteArray {
    return if (str.isEmpty()) {
      ByteArray(0)
    } else {
      str.toByteArray(StandardCharsets.ISO_8859_1)
    }
  }

  /**
   * 将字符串按regex分隔后转换为long数组
   */
  fun toLongArray(str: String, regex: String): LongArray {
    return try {
      val blocks = str.split(Regex(regex)).toTypedArray()
      val result = LongArray(blocks.size)
      var index = 0
      for (block in blocks) {
        result[index++] = block.toLong()
      }
      result
    } catch (e: Exception) {
      LogPrint.logger.error(e.message)
      throw e
    }
  }


}
