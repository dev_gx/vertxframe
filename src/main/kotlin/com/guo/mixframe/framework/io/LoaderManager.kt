package com.guo.mixframe.framework.io

import java.util.concurrent.ConcurrentHashMap

/**
 * @author  gx
 * @description
 */
object LoaderManager {

  private val loaders = ConcurrentHashMap<String, Loader>()

  fun register(key: String, loader: Loader) {
    loaders[key] = loader
  }


  fun reload() {
    loaders.values.forEach{
      it.reload()
    }
  }

}
