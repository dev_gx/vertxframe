package com.guo.mixframe.framework.exceptions;

/**
 * 自定义异常
 */
public class
CustomException extends RuntimeException {
    private static final long serialVersionUID = 2332608236621015980L;

    private final int code;

    public CustomException(int code) {
        super("Exception code is " + code);
        this.code = code;
    }

    /**
     * 取异常码
     */
    public int getCode() {
        return code;
    }
}
