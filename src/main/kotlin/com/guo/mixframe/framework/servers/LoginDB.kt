package com.guo.mixframe.framework.servers

import com.guo.mixframe.framework.session.SessionObject

/**
 * @author  gx
 * @description
 */
abstract class LoginDB {
  abstract suspend fun getSession(sessionId:String): SessionObject?
}
