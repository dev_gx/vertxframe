package com.guo.mixframe.framework.servers

import kotlin.reflect.full.primaryConstructor

/**
 * @author  gx
 * @description
 */
class LoginDBCreator {

  companion object {

    fun createLoginDB(): LoginDB {
      val cls = LoginDB::class
      val constructor = cls.primaryConstructor
      return constructor?.call() as LoginDB
    }
  }


}
