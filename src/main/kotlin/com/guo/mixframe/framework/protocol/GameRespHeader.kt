package com.guo.mixframe.framework.protocol

import com.guo.mixframe.framework.exception.CoreException
import io.protostuff.Tag

/**
 * @author  gx
 * @description
 */
class GameRespHeader {
  @Tag(1)
  var reqId = 0L        //请求id, > 0，正常返回请求, -1 ping请求， 0服务器推送请求

  @Tag(2)
  var isSuccess = true   //请求是否成功

  @Tag(3)
  var dataVersion = 0L  //携带的同步数据版本号

  @Tag(4)
  var errorCode = 0L   //异常错误码

  @Tag(5)
  var errorContent: String? = null  //服务器描述异常的异常描述

  @Tag(6)
  var serverTime = 0L  //服务器时间

  constructor(reqId: Long) {
    this.reqId = reqId
    isSuccess = true
    serverTime = System.currentTimeMillis()
  }

  constructor(reqId: Long, dataVersion: Long) {
    this.reqId = reqId
    this.dataVersion = dataVersion
    isSuccess = true
    serverTime = System.currentTimeMillis()
  }


}
