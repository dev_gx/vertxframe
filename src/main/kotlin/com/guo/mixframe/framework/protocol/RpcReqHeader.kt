package com.guo.mixframe.framework.protocol

import io.protostuff.Tag

/**
 * @author  gx
 * @description
 */
class RpcReqHeader {
  @Tag(1)
  var url: String? = null //请求url

  @Tag(2)
  var rpcType: String? = null //rpc消息分类
}
