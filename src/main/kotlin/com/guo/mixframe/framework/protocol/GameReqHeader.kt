package com.guo.mixframe.framework.protocol

/**
 * @author  gx
 * @description
 */
class GameReqHeader {
  var reqId : Long = 0
  var url:String ? = null
  var session:String? = null
  var lan:String ? = null
}
