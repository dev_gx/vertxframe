package com.guo.mixframe.framework.protocol

import io.protostuff.Tag

/**
 * @author  gx
 * @description
 */
class RpcRespHeader {

  @Tag(1)
  var isSuccess = false //请求是否处理成功

  @Tag(2)
  var errorCode: Long = 0 //请求错误时的异常码

  constructor(isSuccess: Boolean) {
    this.isSuccess = isSuccess
  }

  constructor(errorCode: Long) {
    this.errorCode = errorCode
  }


}
