package com.guo.mixframe.framework.annotations

/**
 * @author  gx
 * @description
 */
@Target(AnnotationTarget.CLASS)
annotation class Receiver
