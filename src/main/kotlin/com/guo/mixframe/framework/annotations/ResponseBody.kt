package com.guo.mixframe.framework.annotations

/**
 * @author  gx
 * @description
 */
@Target(AnnotationTarget.FUNCTION)
annotation class ResponseBody (val url: String, val firstCache: Boolean = false, val ignore: Boolean = false)
