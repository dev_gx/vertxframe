package com.guo.mixframe.framework.annotations

/**
 * @author  gx
 * @description
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Controller(val urlType:Int)
